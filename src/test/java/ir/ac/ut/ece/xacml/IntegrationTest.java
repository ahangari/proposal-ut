package ir.ac.ut.ece.xacml;

import ir.ac.ut.ece.security.xacml.model.Attribute;
import ir.ac.ut.ece.security.xacml.util.XacmlUtil;
import eu.bitwalker.useragentutils.DeviceType;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.authzforce.rest.pdp.jaxrs.XacmlPdpResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@WebMvcTest(XacmlPdpResource.class)
@ContextConfiguration
public class IntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private XacmlPdpResource pdpResource;

    @Autowired
    XacmlUtil util;

    @Test
    public void deviceWindows_Expect_Permit() throws Exception {
        List<String> values = Arrays.asList("Computer");
        Attribute attribute = new Attribute(values, "userDevice");
        List<Attribute> attributes = Arrays.asList(attribute);

        JSONObject result = pdpResource.evaluateJson(util.createXacmlRequest(attributes));
        System.out.println(result);
        assertTrue(result.toString().contains("Permit"));
    }

    @Test
    public void deviceAndroid_Expect_Deny() throws Exception {
        List<String> values = Arrays.asList("Mobile");
        Attribute attribute = new Attribute(values, "userDevice");
        List<Attribute> attributes = Arrays.asList(attribute);

        JSONObject result = pdpResource.evaluateJson(util.createXacmlRequest(attributes));
        System.out.println(result);
        assertTrue(result.toString().contains("Deny"));
    }
}
