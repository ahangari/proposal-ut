package ir.ac.ut.ece.xacml;

import ir.ac.ut.ece.security.xacml.util.XacmlUtil;
import org.ow2.authzforce.core.pdp.impl.PdpEngineConfiguration;
import org.ow2.authzforce.rest.pdp.jaxrs.XacmlPdpResource;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Primary;

import java.io.IOException;

@SpringBootConfiguration
@ImportResource(value = "classpath:spring-beans.xml")
public class ContextConfiguration {

    @Bean
    @Primary
    public PdpEngineConfiguration pdpEngineConfiguration() throws IOException {
        return PdpEngineConfiguration.getInstance("classpath:policy/pdp.xml");
    }

    @Bean
    public XacmlPdpResource xacmlPdpResource() throws IOException {
        return new XacmlPdpResource(pdpEngineConfiguration());
    }

    @Bean
    public XacmlUtil xacmlUtil(){
        return new XacmlUtil();
    }
}
