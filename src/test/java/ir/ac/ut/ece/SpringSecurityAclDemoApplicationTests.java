package ir.ac.ut.ece;

import ir.ac.ut.ece.spring.SpringSecurityAclDemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringSecurityAclDemoApplication.class)
public class SpringSecurityAclDemoApplicationTests {

	@Test
	public void contextLoads() {
	}

}
