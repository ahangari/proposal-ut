package ir.ac.ut.ece.security.xacml.util;

import ir.ac.ut.ece.security.xacml.model.Attribute;
import org.apache.cxf.helpers.IOUtils;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class XacmlUtilTest {


    String DATA_TYPE = "http://www.w3.org/2001/XMLSchema#string";
    String VALUE = "Computer";
    String ATTRIBUTE_ID = "userDevice";
    Boolean INCLUDE_IN_RESULT = Boolean.FALSE;

    String requestJsonStr;

    @Before
    public void setUp() throws IOException, ParseException {
        String json = IOUtils.toString(
                this.getClass().getResourceAsStream("/sample_request.json"),
                "UTF-8"
        );
        JSONParser jsonParser = new JSONParser();
        requestJsonStr = jsonParser.parse(json).toString();
    }

    @Test
    public void testAttributesToString() throws IOException, ParseException {

        XacmlUtil mockXacmlUtil = Mockito.mock(XacmlUtil.class);
        Mockito.doReturn(requestJsonStr).when(mockXacmlUtil).requestJsonFile();
        Mockito.doCallRealMethod().when(mockXacmlUtil).createXacmlRequest(Mockito.anyList());

        Attribute attribute1 = new Attribute(DATA_TYPE, Arrays.asList(VALUE), ATTRIBUTE_ID, INCLUDE_IN_RESULT);
        List<Attribute> attributes = Arrays.asList(attribute1);
        mockXacmlUtil.createXacmlRequest(attributes);
    }
}