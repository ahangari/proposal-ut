package ir.ac.ut.ece.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ir.ac.ut.ece.security.xacml.annotations.XacmlResourceAttribute;
import ir.ac.ut.ece.validation.PasswordMatches;
import ir.ac.ut.ece.validation.ValidPassword;
import eu.bitwalker.useragentutils.DeviceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@PasswordMatches
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String code;

    @Email
    @NotEmpty(message = "Email is required.")
    @NotNull
    private String email;

    @ValidPassword
    @NotEmpty(message = "Password is required.")
    @NotNull
    private String password;

    @Transient
    @NotEmpty(message = "Password confirmation is required.")
    @NotNull
    private String passwordConfirmation;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "USER_ROLE",
            joinColumns = {@JoinColumn(name = "FK_USER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "FK_ROLE_ID")}
    )
    private Set<Role> roles;

    //
    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "DEVICE_TYPE")
    @JsonIgnore
    @XacmlResourceAttribute
    private DeviceType userDeviceType;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((email == null) ? 0 : email.hashCode());
        result = (prime * result) + ((id == null) ? 0 : id.hashCode());
        result = (prime * result) + ((password == null) ? 0 : password.hashCode());
        result = (prime * result) + ((passwordConfirmation == null) ? 0 : passwordConfirmation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;

        if (email == null) {
            if (other.email != null) {
                return false;
            }
        } else if (!email.equals(other.email)) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        if (passwordConfirmation == null) {
            if (other.passwordConfirmation != null) {
                return false;
            }
        } else if (!passwordConfirmation.equals(other.passwordConfirmation)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("User [id=").append(id).append(", email=").append(email).append("]");
        return builder.toString();
    }

}
