package ir.ac.ut.ece.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * @author kawasima
 */
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
public class Role {
    @Id
    @GeneratedValue
    @Getter
    @Setter
    private Long id;

    @NonNull
    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "ROLE_PERMISSION",
            joinColumns = {@JoinColumn(name = "FK_PERMISSION_ID")},
            inverseJoinColumns = {@JoinColumn(name = "FK_ROLE_ID")}
            )
    private Set<Permission> permissions;

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        } else if (name != null) {
            return name.hashCode();
        }

        return 0;
    }

    @Override
    public boolean equals(Object another) {
        if (another == null || !(another instanceof Role))
            return false;

        Role anotherRole = (Role) another;

        return anotherRole.id != null && (anotherRole.id == this.id);
    }

    @Override
    public String toString() {
        return name;
    }
}
