package ir.ac.ut.ece.model;

public interface IEntity {

    public Long getId();
}
