package ir.ac.ut.ece.persistence;

import ir.ac.ut.ece.model.Possession;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PossessionRepository extends JpaRepository<Possession, Long> {

    Possession findByName(String name);

}
