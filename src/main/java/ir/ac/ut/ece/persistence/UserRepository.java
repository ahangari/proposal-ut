package ir.ac.ut.ece.persistence;

import ir.ac.ut.ece.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

}
