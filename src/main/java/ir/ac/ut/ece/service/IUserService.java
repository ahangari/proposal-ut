package ir.ac.ut.ece.service;

import ir.ac.ut.ece.model.User;
import ir.ac.ut.ece.validation.EmailExistsException;

public interface IUserService {

    User registerNewUser(User user) throws EmailExistsException;

    User findUserByEmail(String email);

}
