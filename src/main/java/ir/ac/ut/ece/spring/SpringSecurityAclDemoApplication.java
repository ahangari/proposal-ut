package ir.ac.ut.ece.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("ir.ac.ut.ece")
@EnableJpaRepositories("ir.ac.ut.ece")
@EntityScan("ir.ac.ut.ece.model")
public class SpringSecurityAclDemoApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(new Class[]{SpringSecurityAclDemoApplication.class, SecurityConfiguration.class, WebMvcConfiguration.class, MethodSecurityConfiguration.class}, args);
    }

}
