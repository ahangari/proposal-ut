package ir.ac.ut.ece.security.xacml.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Decision {
    @JsonProperty("Decision")
    private DecisionType decision;
}
