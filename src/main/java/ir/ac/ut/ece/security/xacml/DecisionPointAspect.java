package ir.ac.ut.ece.security.xacml;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.ac.ut.ece.model.Possession;
import ir.ac.ut.ece.model.User;
import ir.ac.ut.ece.security.xacml.DTO.Decision;
import ir.ac.ut.ece.security.xacml.DTO.DecisionType;
import ir.ac.ut.ece.security.xacml.DTO.Response;
import ir.ac.ut.ece.security.xacml.model.Attribute;
import ir.ac.ut.ece.security.xacml.util.XacmlUtil;
import ir.ac.ut.ece.service.UserService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.json.JSONObject;
import org.ow2.authzforce.rest.pdp.jaxrs.XacmlPdpResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.ws.rs.NotAuthorizedException;
import java.security.Principal;
import java.util.List;

@Aspect
@Component
public class DecisionPointAspect {

    @Autowired
    private XacmlPdpResource xacmlPdpResource;

    @Autowired
    UserService userService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    XacmlUtil xacmlUtil;

    @Autowired
    AnnotationProcessor annotationProcessor;

    @AfterReturning(pointcut = "@annotation(ir.ac.ut.ece.security.xacml.annotations.XacmlDecisionPost)", returning = "retVal")
    public Object evaluateAuthorization(Object retVal) throws Throwable {
        Possession result = (Possession) retVal;
        User user = getUserAuthenticated();
        List<Attribute> attributes = annotationProcessor.findResourceAttributes(user);
        JSONObject responseJson = xacmlPdpResource.evaluateJson(xacmlUtil.createXacmlRequest(attributes));
        Response responses = objectMapper.readValue(responseJson.toString(), Response.class);
        List<Decision> decisions = responses.getDecisions();
        if(decisions.get(0).getDecision() == DecisionType.Deny){
            result.setDetails(null);
        }
        return result;
    }

    private User getUserAuthenticated() {
        Principal principal = SecurityContextHolder.getContext().getAuthentication();
        return userService.findUserByEmail(principal.getName());
    }

    @Around("@annotation(ir.ac.ut.ece.security.xacml.annotations.XacmlDecisionPre)")
    public void preAuthorization(ProceedingJoinPoint joinPoint) throws Throwable {
        User user = getUserAuthenticated();
        List<Attribute> attributes = annotationProcessor.findResourceAttributes(user);
        JSONObject responseJson = xacmlPdpResource.evaluateJson(xacmlUtil.createXacmlRequest(attributes));
        Response responses = objectMapper.readValue(responseJson.toString(), Response.class);
        List<Decision> decisions = responses.getDecisions();
        if(decisions.get(0).getDecision() == DecisionType.Deny){
            throw new NotAuthorizedException(user, "You Do Not Have Access!");
        }
        joinPoint.proceed();
    }
}
