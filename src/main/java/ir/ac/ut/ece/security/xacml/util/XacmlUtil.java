package ir.ac.ut.ece.security.xacml.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.ac.ut.ece.security.xacml.model.Attribute;
import org.apache.cxf.helpers.IOUtils;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class XacmlUtil {

    @Value("classpath:policy/sample_request.json")
    Resource resourceFile;

    public JSONObject createXacmlRequest(List<Attribute> attributes) {
        try {
            String attributesString = attributesToString(attributes);
            String requestStr = requestJsonFile();
            requestStr = requestStr.replace("\"${attributes}\"", attributesString);
            JSONObject requestObject = new JSONObject(requestStr);
            return requestObject;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String attributesToString(List<Attribute> attributes) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(attributes);
    }

    public String requestJsonFile() throws IOException, ParseException {
        String json = IOUtils.toString(
                resourceFile.getInputStream(),
                "UTF-8"
        );
        JSONParser jsonParser = new JSONParser();
        return jsonParser.parse(json).toString();
    }
}
