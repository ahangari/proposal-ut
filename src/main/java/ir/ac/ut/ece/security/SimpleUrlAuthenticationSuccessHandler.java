package ir.ac.ut.ece.security;

import ir.ac.ut.ece.model.User;
import ir.ac.ut.ece.service.UserService;
import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class SimpleUrlAuthenticationSuccessHandler
        implements AuthenticationSuccessHandler {

    @Autowired
    UserService userService;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();


    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication)
            throws IOException {
        String userAgentStr = request.getHeader("User-Agent");
        org.springframework.security.core.userdetails.User userSpring = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
        User user = userService.findUserByEmail(userSpring.getUsername());
        user.setUserDeviceType(new UserAgent(userAgentStr).getOperatingSystem().getDeviceType());
        userService.save(user);
        redirectStrategy.sendRedirect(request, response, "/");
    }
}
