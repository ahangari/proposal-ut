package ir.ac.ut.ece.security.xacml.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Attribute {

    public Attribute(List<String> value, String attributeId){
        this.value = value;
        this.attributeId = attributeId;
    }

    @JsonProperty("DataType")
    String dataType = "http://www.w3.org/2001/XMLSchema#string";

    @JsonProperty("Value")
    List<String> value;

    @JsonProperty("AttributeId")
    String attributeId = "userDevice";

    @JsonProperty("IncludeInResult")
    Boolean includeInResult = Boolean.FALSE;
}
