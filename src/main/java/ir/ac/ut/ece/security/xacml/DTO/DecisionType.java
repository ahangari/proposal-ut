package ir.ac.ut.ece.security.xacml.DTO;

public enum DecisionType {
    Permit, Deny
}
