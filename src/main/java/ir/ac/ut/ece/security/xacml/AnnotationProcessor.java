package ir.ac.ut.ece.security.xacml;

import eu.bitwalker.useragentutils.DeviceType;
import ir.ac.ut.ece.model.User;
import ir.ac.ut.ece.security.xacml.annotations.XacmlResourceAttribute;
import ir.ac.ut.ece.security.xacml.model.Attribute;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.*;

@Component
public class AnnotationProcessor {


    public List<Attribute> findResourceAttributes(User user) throws IllegalAccessException {
        Class<?> clazz = user.getClass();
        Map<Field, Object> xacmlResourceAttributes = new HashMap();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(XacmlResourceAttribute.class)) {
                xacmlResourceAttributes.put(field, field.get(user));
            }
        }
        List<Attribute> attributes = processAttributes(xacmlResourceAttributes);
        return attributes;
    }

    private List<Attribute> processAttributes(Map<Field, Object> xacmlResourceAttributes) {
        List<Attribute> attributes = new ArrayList<>();
        for (Map.Entry<Field, Object> entry : xacmlResourceAttributes.entrySet()){
            Attribute attribute = new Attribute();
            attribute.setAttributeId(entry.getKey().getName());
            Object value = entry.getValue();
            if(value instanceof DeviceType)
                attribute.setValue(Arrays.asList(((DeviceType) value).getName()));
            attributes.add(attribute);
        }
        return attributes;
    }
}
